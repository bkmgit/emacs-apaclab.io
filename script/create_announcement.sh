#!/usr/bin/env bash

# This script needs to be run from 08th to 14th of a month (cron: 0 0
# 08-14 * *). It runs only on a Saturday from this date range.
# https://stackoverflow.com/a/11683643/6202405

set -o errexit -o pipefail
set -x

if [[ "$(date +%a)" != "Sat" ]]; then
  echo "It's not a Saturday today: $(date)"
  exit 0
fi

ann_dir="content/announcements"

# Second Saturday from now i.e. 4th Saturday of the month.
second_sat="2 Saturday 14:00"
iso_8601_date="$(date --date "${second_sat}" --iso-8601=seconds)"
date_day="$(date --date "${second_sat}" +%d)"

ann_file="${ann_dir}/$(date "+%B-%Y" | tr '[:upper:]' '[:lower:]').md"

make announcement
sed -i "s/<meetup_date>/${date_day}/g" "${ann_file}"
sed -i "s/^event_date: .*/event_date: ${iso_8601_date}/g" "${ann_file}"

echo "${ann_file}" > "files_added"
