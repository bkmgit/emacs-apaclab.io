---
title: "CANCELED Emacs Asia-Pacific (APAC) virtual meetup, Saturday, November 27, 2021"
linktitle: "CANCELED November 2021 virtual meetup"
date: 2021-11-13T00:27:56Z
event_date: 2021-11-27T14:00:00+00:00
categories:
- Event
---

[Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io) virtual
meetup will not happening this month. You can join us for the
EmacsConf 2021:

- [EmacsConf 2021 | Online Conference | November 27 and 28, 2021
  (Sat-Sun)](https://emacsconf.org/2021/) from [1900 IST](# "07:00 PM
  Indian Standard Time. UTC+05:30") onwards.
- [28th Nov 2021: EmacsConf 2021 Alternate Stream for
  APAC](https://libreau.org/upcoming.html#emacsconf21) from [0530
  IST](# "05:30 AM Indian Standard Time. UTC+05:30") onwards.
