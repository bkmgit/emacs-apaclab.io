---
title: "Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, February 27, 2021"
linktitle: "February 2021 virtual meetup"
date: 2021-02-13T00:07:24Z
event_date: 2021-02-27T14:00:00+00:00
categories:
- Event
---

This month's [Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io)
virtual meetup is scheduled for Saturday, February
27, 2021 with Jitsi Meet and `#emacs` on
Freenode IRC. The timing will be [1400 to 1530 IST](# "02:00 PM Indian
Standard Time. UTC+05:30").

<!-- *The meetup might get extended by 30 minutes if there is any talk, this -->
<!-- page will be updated accordingly.* -->

### Talks
- **org-clock-csv by Shakthi Kannan (~15m)**  
  org-clock-csv exports clock entries from Org-mode to CSV. We would
  like to use the same with Operation Blue Moon (OBM) project, and
  will discuss its use and required features for integration.  
  - <https://github.com/atheriel/org-clock-csv>
  - <https://gitlab.com/shakthimaan/operation-blue-moon>

If you would like to give a demo or talk (maximum 20 minutes) on GNU
Emacs or any variant, please contact `bhavin192` on Freenode with your
talk details:

- Topic
- Description
- Duration
- About Yourself

The Jitsi Meet (video conferencing) URL for the session will be posted
on Freenode IRC channels `#emacs`, `#ilugc` and `#emacsconf`, 30
minutes prior to the meeting, and also on the [ILUGC mailing
list](https://www.freelists.org/list/ilugc) on the day of the
meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).
