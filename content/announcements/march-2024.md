---
title: "UPDATED Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, March 23, 2024"
linktitle: "UPDATED March 2024 virtual meetup"
date: 2024-03-09T00:01:03Z
event_date: 2024-03-23T13:00:00+00:00
categories:
- Event
---

*Note: the timing has been updated for this month's meetup.*

This month's [Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io)
virtual meetup is scheduled for Saturday, March
23, 2024 with BigBlueButton and `#emacs`
on Libera Chat IRC. The timing will be [1300 to 1400 IST](# "01:00 PM
Indian Standard Time. UTC+05:30").

*The meetup might get extended by 30 minutes if there is any talk, this
page will be updated accordingly.*

<!-- ### Talks -->
<!-- - **[title] by [speaker] (~20m)**   -->
<!--   [description]. -->

If you would like to give a demo or talk (maximum 20 minutes) on GNU
Emacs or any variant, please contact `bhavin192` on Libera Chat with
your talk details:

- Topic
- Description
- Duration
- About Yourself

The BigBlueButton (video conferencing) URL for the session:
[https://emacs-apac.gitlab.io/r/bbb/](https://emacs-apac.gitlab.io/r/bbb/
"Redirects to https://bbb.emacsverse.org/b/bha-lms-tqi-dtx")

The URL will also be posted on Libera Chat IRC channels `#emacs`,
`#ilugc` and `#emacsconf`, 30 minutes prior to the meeting, and on the
[ILUGC mailing list](https://www.freelists.org/list/ilugc) on the day
of the meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).
