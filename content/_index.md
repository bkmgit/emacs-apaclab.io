---
title: "Emacs APAC"
date: 2020-12-13T17:05:24+05:30
---

# About Emacs APAC
We are Emacs enthusiasts who live in the Asia-Pacific (APAC)
time-zone. We meet every fourth Saturday of the month
(`<%%(diary-float t 6 4)>` in Org mode).

## Where
The event is scheduled virtually using BigBlueButton at 1400 Indian
Standard Time (IST). The meeting URL is mentioned in the announcement
posts. It is also posted on Libera Chat IRC channels `#emacs`,
`#ilugc` and `#emacsconf`, 30 minutes prior to the meeting, and on the
[ILUGC mailing list](https://www.freelists.org/list/ilugc) on the day
of the meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).

Checkout the upcoming meetings [here](#upcoming).

## Talks
We usually have free flowing discussions around new Emacs packages /
features discovered, issues faced, experiences, usage tips, resources
for further learning etc. These are related to GNU Emacs and its
variants. Sometimes people also share their screens and give demos of
their Emacs setup and use.

If you would like to give a talk (20 minutes maximum), please send an
email to TODO with the details.

## Frequently Asked Questions
- **I'm new to Emacs, can / should I join?**  
    Yes! You are always welcome. We have participants from different
    walks of life with varied experiences in Emacs. You can ask your
    questions and the attendees will be able to help, at least point
    you in the right direction.

- **Which language is used for communication?**  
    English.

- **I'm not from APAC, can I join?**  
    Definitely! If the timing is suitable for you, please join.
