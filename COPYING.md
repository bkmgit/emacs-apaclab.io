---
title: "COPYING"
---

Except where otherwise noted, the material on the Emacs APAC website
are licensed under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Copy of the license is included in the Emacs APAC website repository,
in the
[LICENSE](https://gitlab.com/emacs-apac/emacs-apac.gitlab.io/-/blob/master/LICENSE)
file.

By contributing to any page on this website, you agree to make your
contributions available under the above license. You are also
promising that you are the author of your changes, or that you copied
them from a work in the public domain or a work released under a free
license that is compatible with the above license. DO NOT SUBMIT
COPYRIGHTED WORK WITHOUT PERMISSION.

The Emacs APAC website uses the Hugo theme
[smol](https://github.com/colorchestra/smol) which is licensed under
the [MIT
license](https://github.com/colorchestra/smol/blob/master/LICENSE.md).

This page is taken from [EmacsConf
wiki](https://emacsconf.org/COPYING/) which is licensed under
[GPLv3+](https://emacsconf.org/COPYING.GPL).
